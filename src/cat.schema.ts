import * as mongose from 'mongoose';

export const CatShema = new mongose.Schema({
  name: String,
  age: Number,
  breed: String,
});
