import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Mensaje } from '../entities/mensaje.entity';
import { Repository } from 'typeorm';
import { CreateMensajeDto } from '../dto/create-mensaje-dto';

@Injectable()
export class MensajesService {

  constructor(
    @InjectRepository(Mensaje)
    private readonly mensajeRepository: Repository<Mensaje>,
  ) { }

  async getAll() {
    return await this.mensajeRepository.find();
  }

  async getOne(id: number) {
    return await this.mensajeRepository.findOne(id);
  }

  async createMensaje(mensaje: CreateMensajeDto) {
    return await this.mensajeRepository.save(mensaje);
  }

  async updateMensaje(newMensaje: CreateMensajeDto, id: number) {
    const mensaje = await this.mensajeRepository.findOne(id);
    mensaje.mensaje = newMensaje.mensaje;
    mensaje.nick = newMensaje.nick;

    return this.mensajeRepository.save(mensaje);
  }

  async deleteMensaje(id: number) {
    return await this.mensajeRepository.delete(id);
  }
}
