import { Controller, Post, Body, Get, Put, Delete, Res, HttpStatus, Param, Header } from '@nestjs/common';
import { CreateMensajeDto } from '../DTO/create-mensaje-dto';
import { MensajesService } from '../services/mensajes.service';

@Controller('mensajes')
export class MensajesController {

  constructor(private mensajeService: MensajesService) {

  }

  @Post()
  create(@Body() mensaje: CreateMensajeDto, @Res() response) {
    this.mensajeService.createMensaje(mensaje).then(resp => {
      response.status(HttpStatus.CREATED).json(mensaje);
    })
      .catch(() => {
        response.status(HttpStatus.FORBIDDEN).json({ mensaje: 'Error en la creacion del mensaje' });
      });
  }

  @Get()
  getAll(@Res() response) {
    this.mensajeService.getAll().then(mensajesList => {
      response.status(HttpStatus.OK).json(mensajesList);
    }).catch(() => {
      response.status(HttpStatus.FORBIDDEN).json({ mensaje: 'error al consultar data' });
    });
  }

  @Get(':id')
  getOne(@Res() response, @Param('id') id) {
    this.mensajeService.getOne(parseInt(id)).then(mensajesList => {
      response.status(HttpStatus.OK).json(mensajesList);
    }).catch(() => {
      response.status(HttpStatus.FORBIDDEN).json({ mensaje: 'error al consultar data' });
    });
  }



  @Put(':id')
  updateMensaje(@Body() updateMensajeDTO: CreateMensajeDto, @Res() response, @Param('id') idMensaje) {
    this.mensajeService.updateMensaje(updateMensajeDTO, idMensaje).then(mensaje => {
      response.status(HttpStatus.OK).json(updateMensajeDTO);
    }).catch(() => {
      response.status(HttpStatus.FORBIDDEN).json({ mensaje: 'error al actualizar mensaje' });
    });
  }

  @Delete(':id')
  destoy(@Res() response, @Param('id') idMensaje) {
    this.mensajeService.deleteMensaje(idMensaje).then(mensaje => {
      response.status(HttpStatus.OK).json(mensaje);
    }).catch(() => {
      response.status(HttpStatus.FORBIDDEN).json({ mensaje: 'error al eliminar mensaje' });
    });
  }

}
