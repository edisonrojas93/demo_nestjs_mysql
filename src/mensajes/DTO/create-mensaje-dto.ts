import { IsString, IsInt, IsNotEmpty, Min, Max, IsNumberString, } from "class-validator";


export class CreateMensajeDto {
  @IsString()
  @IsNotEmpty({
    message: 'es requerido',
  })
  nick: string;

  @IsString()
  @IsNotEmpty({
    message: 'es requerido',
  })
  mensaje: string;
}
